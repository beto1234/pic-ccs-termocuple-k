/*
++++++++++Conectando una Thermocouple tipo K al PIC16f873++++++++++++++++++++++
+++++++++++ By Alberto Roobledo S�nches - estudia en UTN FRRE ++++++++++++++++++
++++++++++++++++++++++WWW.CHACOELECNOR.COM.AR+++++++++++++++++++++++++++++++++++
*/
#include <16F873a.h>
/* ********************* Conversor A/D de 10 bits *************************** */
#device adc=10 
#FUSES NOWDT, XT, PUT, NOPROTECT, NODEBUG, NOBROWNOUT, NOLVP, NOCPD, NOWRT  
#use delay(clock=4000000)
#define use_portb_lcd TRUE
#include <LCD.c>


//VARIABLED GLOBALES
int16 TE,LM;
float Tc,Tlm,mVO,Vm,Vlm,Tth;
float a1=2.508355E+01  ,a2=7.860106E-02  ,a3=-2.503131E-01  ,a4=8.315270E-02  ,a5=-1.228034E-02  ,a6=9.804036E-04 ,a7=-4.413030E-05  ,a8=1.057734E-06  ,a9=-1.052755E-08 ;
   //0�a a 500 �c - 0.000 to 20.644mv
float a11=-1.318058E+02 ,a12=4.830222E+01  ,a13=-1.646031E+00  ,a14=5.464731E-02  ,a15=-9.650715E-04  ,a16=8.802193E-06  ,a17=-3.110810E-08 ;
   //500�c a 1372 �c    20.644 to 54.886
void main(void){
  setup_adc_ports(AN0_AN1_AN3);
  setup_adc(ADC_CLOCK_INTERNAL);
lcd_init();
lcd_putc("\f");
lcd_gotoxy(1,1);
lcd_putc("Tc   Tlm  Therm.");
   while(true){
   set_adc_channel(0);
   delay_us(20);
   TE=read_adc();
   //setup_adc (adc_off);   //Apaga ADC
   delay_us(50);
   set_adc_channel(1);
   delay_us(20);
   LM=read_adc();
   Vlm = (float) 5000 * LM / 1024 ; // tencion de entrada en mV
   Tlm= (float)Vlm/10; 
   //Comienso de la conversion de los valores
   mVO =(float) 5000 * TE / 1024 ; // tencion de entrada en mV
   Vm =(float) mVO / (1+(100000/1000)); // Tencion en la salida de la termocupla
   // Pasamos la tencion medida Vm a temperatura gracias a la ecuacion con los coeficientes de la tabla
  
   //formula tipo:   t_90 = a_0 + a_1*E + a_2*E^2 + ...+ a_n*E^n,

   if(Vm<20.6)
   {
    Tc =(float) Vm * (a1+ Vm* (a2 +Vm*(a3+Vm*(a4+Vm*(a5+Vm*(a6+Vm*(a7+Vm*((a8+Vm*(a9))))))))));
   }

   else if(Vm>20.6)
   {

    Tc=(float)(a11 + Vm*(a12+Vm*(a13+Vm*(a14+Vm*(a15+Vm*(a16+Vm*(a17)))))));

   }
   Tth=(float)Tc+Tlm;
   //  mostrar en el lcd
   lcd_gotoxy(12,2);
   printf(lcd_putc,"%3.1f",Tth);
   delay_us(20);
   lcd_gotoxy(7,2);
   printf(lcd_putc,"%2.1f",Tlm);
   delay_us(20);
   lcd_gotoxy(1,2);
   printf(lcd_putc,"%3.1f",Tc);
 
   }
}


